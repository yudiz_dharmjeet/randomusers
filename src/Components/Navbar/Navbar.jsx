import React from "react";
import { NavLink, useNavigate } from "react-router-dom";
import { UserState } from "../../Context/Context";

import styles from "./Navbar.module.scss";

function Navbar() {
  const { state, dispatch } = UserState();

  const navigate = useNavigate();

  function handleLogout() {
    dispatch({ type: "LOGOUT" });
    navigate("/");
  }

  function handleLogoClick() {
    navigate("/");
  }

  return (
    <div className={styles.navbar}>
      <nav>
        <div className={styles.logo} onClick={handleLogoClick}>
          RandomUsers
        </div>
        <ul className={styles.list}>
          <li>
            <NavLink
              className={(navData) =>
                navData.isActive ? styles.active : styles.link
              }
              to="/"
            >
              Home
            </NavLink>
          </li>
          {state.loggedIn && (
            <>
              <li>
                <NavLink
                  className={(navData) =>
                    navData.isActive ? styles.active : styles.link
                  }
                  to="/user"
                >
                  Users
                </NavLink>
              </li>
              <li>
                <NavLink className={styles.link} to="/" onClick={handleLogout}>
                  Logout
                </NavLink>
              </li>
            </>
          )}
          {!state.loggedIn && (
            <li>
              <NavLink
                className={(navData) =>
                  navData.isActive ? styles.active : styles.link
                }
                to="/login"
              >
                Login
              </NavLink>
            </li>
          )}
        </ul>
      </nav>
    </div>
  );
}

export default Navbar;
