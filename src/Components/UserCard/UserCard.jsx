import React from "react";

import PropTypes from "prop-types";

import styles from "./UserCard.module.scss";
import { useNavigate } from "react-router-dom";

function UserCard({ user }) {
  const navigate = useNavigate();

  function handleClick() {
    navigate(`/user/${user.login?.uuid}`);
  }

  return (
    <div className={styles.card}>
      <div className={styles.imgbox}>
        <img src={user.picture?.large} alt={user.name?.first} />
      </div>
      <div className={styles.desc}>
        <h3>
          {user.name?.title} {user.name?.first} {user.name?.last}
        </h3>
        <button className={styles.btn} onClick={handleClick}>
          Know More
        </button>
      </div>
    </div>
  );
}

UserCard.propTypes = {
  user: PropTypes.object,
};

export default UserCard;
