import React from "react";
import { UserState } from "../../Context/Context";
import UserCard from "../UserCard/UserCard";

import styles from "./UsersList.module.scss";

function UsersList() {
  const { state } = UserState();

  return (
    <div className={styles.list}>
      {state.users.map((user) => (
        <UserCard key={user.login?.uuid} user={user} />
      ))}
    </div>
  );
}

export default UsersList;
