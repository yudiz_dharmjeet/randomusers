import React from "react";
import { Navigate, Outlet } from "react-router-dom";
import { UserState } from "../Context/Context";

function PrivateRoute() {
  const { state } = UserState();

  return state.loggedIn ? <Outlet /> : <Navigate to="/login" />;
}

export default PrivateRoute;
