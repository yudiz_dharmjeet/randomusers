import React, { createContext, useContext, useReducer } from "react";

import PropTypes from "prop-types";
import { reducers } from "./Reducers";

const User = createContext();

function Context({ children }) {
  const [state, dispatch] = useReducer(reducers, {
    users: [],
    loggedInUser: {},
    loggedIn: false,
  });

  return <User.Provider value={{ state, dispatch }}>{children}</User.Provider>;
}

Context.propTypes = {
  children: PropTypes.node,
};

export default Context;

export const UserState = () => {
  return useContext(User);
};
