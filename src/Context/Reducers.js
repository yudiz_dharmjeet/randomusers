export const reducers = (state, action) => {
  switch (action.type) {
    case "FETCH_USERS":
      return {
        ...state,
        users: action.payload,
      };

    case "LOGGEDIN":
      return {
        ...state,
        loggedIn: true,
        loggedInUser: {
          ...state.loggedInUser,
          username: action.payload.username,
          password: action.payload.password,
        },
      };

    case "LOGOUT":
      return {
        ...state,
        loggedIn: false,
        loggedInUser: {},
      };

    default:
      return state;
  }
};
