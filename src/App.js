import axios from "axios";
import React, { useEffect } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Navbar } from "./Components";
import { UserState } from "./Context/Context";
import { Home, Login, SingleUser, Users } from "./Pages";
import PrivateRoute from "./Routes/PrivateRoute";

function App() {
  const { dispatch } = UserState();

  useEffect(() => {
    async function getUsers() {
      try {
        const response = await axios.get(
          "https://randomuser.me/api/?results=100"
        );
        if (response.status == 200) {
          dispatch({ type: "FETCH_USERS", payload: response.data.results });
        }
      } catch (error) {
        console.log(error);
      }
    }

    getUsers();
  }, []);

  return (
    <BrowserRouter>
      <Navbar />
      <Routes>
        <Route exact path="/login" element={<Login />} />
        <Route exact path="/" element={<Home />} />
        <Route exact path="/user" element={<PrivateRoute />}>
          <Route exact path="/user" element={<Users />} />
          <Route exact path="/user/:id" element={<SingleUser />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
