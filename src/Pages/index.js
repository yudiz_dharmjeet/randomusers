import Users from "./Users/Users";
import SingleUser from "./SingleUser/SingleUser";
import Login from "./Login/Login";
import Home from "./Home/Home";

export { Users, SingleUser, Login, Home };
