import React, { useEffect, useRef } from "react";
import { useNavigate } from "react-router-dom";
import { UserState } from "../../Context/Context";

import styles from "./Login.module.scss";

function Login() {
  const username = useRef(null);
  const password = useRef(null);

  const { dispatch } = UserState();

  const navigate = useNavigate();

  function handleSubmit(e) {
    e.preventDefault();
    if (username.current.value == "") {
      alert("Please enter Username");
    } else if (password.current.value == "") {
      alert("Please enter Password");
    } else if (username.current.value.length < 3) {
      alert("Please enter atlease 3 character in username!");
    } else if (password.current.value.length < 3) {
      alert("Please enter atlease 3 character in password!");
    } else {
      dispatch({
        type: "LOGGEDIN",
        payload: {
          username: username.current.value,
          password: password.current.value,
        },
      });
    }
    navigate("/user");
  }

  useEffect(() => {
    username.current.focus();
  }, []);

  return (
    <div className={styles.login}>
      <form onSubmit={handleSubmit}>
        <h1>Login</h1>
        <input
          type="text"
          name="username"
          id="username"
          placeholder="Username"
          ref={username}
        />
        <input
          type="password"
          name="password"
          id="password"
          placeholder="Password"
          ref={password}
        />
        <button>Login</button>
      </form>
    </div>
  );
}

export default Login;
