import React from "react";
import { useNavigate } from "react-router-dom";

import styles from "./Home.module.scss";

function Home() {
  const navigate = useNavigate();

  function handleGoToUsers() {
    navigate("/user");
  }

  return (
    <div className={styles.home}>
      <div className={styles.box}>
        <h1>Welcome to the RandomUsers!</h1>
        <h4>Check it out the users</h4>
        <button className={styles.btn} onClick={handleGoToUsers}>
          Go To Users
        </button>
      </div>
    </div>
  );
}

export default Home;
