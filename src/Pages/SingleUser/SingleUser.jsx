import React from "react";
import { useNavigate, useParams } from "react-router-dom";
import { UserState } from "../../Context/Context";

import { AiFillMail } from "react-icons/ai";
import { HiLocationMarker } from "react-icons/hi";
import { BsFillTelephoneFill, BsFillPersonFill } from "react-icons/bs";

import styles from "./SingleUser.module.scss";

function SingleUser() {
  const params = useParams();
  const navigate = useNavigate();

  const { state } = UserState();

  let myUser = state.users.find((user) => {
    return user.login?.uuid == params.id;
  });

  function handleGoBack() {
    navigate(-1);
  }

  return (
    <div className={styles.user}>
      <div className={styles.card}>
        <div className={styles.picture}>
          <img src={myUser.picture.large} alt={myUser.name.first} />
        </div>
        <div className={styles.desc}>
          <h1>
            {myUser.name.title} {myUser.name.first} {myUser.name.last}
          </h1>
          <h4 className={styles.item}>
            <AiFillMail className={styles.icon} /> {myUser.email}
          </h4>
          <h4 className={styles.item}>
            <HiLocationMarker className={styles.icon} /> {myUser.location.city},{" "}
            {myUser.location.state}, {myUser.location.country}
          </h4>
          <h4 className={styles.item}>
            <BsFillTelephoneFill className={styles.icon} /> {myUser.phone}
          </h4>
          <h4 className={styles.item}>
            <BsFillPersonFill className={styles.icon} /> {myUser.dob.age} years
            old
          </h4>
        </div>
      </div>

      <button className={styles.btn} onClick={handleGoBack}>
        Go Back
      </button>
    </div>
  );
}

export default SingleUser;
