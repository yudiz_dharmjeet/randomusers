import React from "react";
import { UsersList } from "../../Components";

import styles from "./Users.module.scss";

function Users() {
  return (
    <div className={styles.home}>
      <UsersList />
    </div>
  );
}

export default Users;
